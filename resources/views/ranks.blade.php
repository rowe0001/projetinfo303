<x-guest-layout>
    @if (Route::has('login'))
        <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
            @auth
                <a href="{{ url('/dashboard') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Tableau de bord</a>
            @else
                <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <div class="container mx-auto mt-8" style="margin-left: 3rem; margin-right: 3rem;">
        <h1 class="text-3xl font-bold mb-4">Team Rankings</h1>
    
        @forelse ($teams as $key => $team)
            <div class="max-w-md mx-auto dark:bg-gray-800 rounded-lg shadow-md p-6 mb-4">
                <div class="flex justify-between mb-2">
                    <h2 class="text-xl font-bold dark:text-white">{{ $team->name }}</h2>
                    <span class="text-gray-500">#{{ $key + 1 }}</span>
                </div>
                <p class="text-gray-600">{{ $team->description }}</p>
                <p class="text-gray-500">Medailles: {{ $team->medals }}</p>
                <p class="text-gray-500">Points: {{ $team->points }}</p>
                @if ($team->leader_id)
                    <p class="text-gray-500">Leader: {{ optional($leaders[$team->leader_id])->name ?? 'N/A' }}</p>
                @else
                    <p class="text-gray-500">Leader: None</p>
                @endif
            </div>
        @empty
            <p>No teams available.</p>
        @endforelse
    </div>
</x-guest-layout>