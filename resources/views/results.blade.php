<x-guest-layout>
    @if (Route::has('login'))
        <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
            @auth
                <a href="{{ url('/dashboard') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Tableau de bord</a>
            @else
                <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <div class="container mx-auto mt-8" style="margin-left: 3rem; margin-right: 3rem;">
        @foreach ($rankings as $key => $ranking)
            @if (count($ranking) > 0)
                <div class="dark:bg-gray-800 rounded-lg shadow-md p-6 mt-4">
                    <div class="flex justify-between">
                        <h2 class="text-xl font-bold dark:text-white">{{$events[$key]}}</h2>
                    </div>
                    <div class="mt-4">
                        @foreach ($ranking as $positions)
                            <div class="flex justify-between">
                                <span class="text-gray-500">{{$constitutives[$positions->constitutive_id]}}</span>
                                <span class="text-gray-500">
                                    &#129351: {{$positions->first_positions}} 
                                    &#129352: {{$positions->second_positions}} 
                                    &#129353: {{$positions->third_positions}}
                                </span>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</x-guest-layout>