<x-guest-layout>
    @if (Route::has('login'))
        <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
            @auth
                <a href="{{ url('/dashboard') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Tableau de bord</a>
            @else
                <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <div class="container mx-auto mt-8" style="margin-left:3rem; margin-right:3rem;">
        <h1 class="text-3xl font-bold mb-4">Latest News</h1>

        @forelse ($newsList as $news)
            <div class="max-w-md mx-auto dark:bg-gray-800 rounded-lg shadow-md p-6 mb-4">
                <h2 class="text-xl font-bold mb-2 dark:text-white">{{ $news->name }}</h2>
                <p class="text-gray-600">{{ $news->description }}</p>
                <p class="text-gray-500">{{ $news->location }}</p>
                <p class="text-gray-500">Start Date: {{ $news->start_date->format('F j, Y H:i:s') }}</p>
                <p class="text-gray-500">End Date: {{ $news->end_date->format('F j, Y H:i:s') }}</p>
                <p class="text-gray-500">Max Attendees: {{ $news->max_attendees }}</p>
            </div>
        @empty
            <p>No news available.</p>
        @endforelse
    </div>
</x-guest-layout>