<x-guest-layout>
    @if (Route::has('login'))
        <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
            @auth
                <a href="{{ url('/dashboard') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Tableau de bord</a>
            @else
                <a href="{{ route('login') }}" class="font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <div>
        <section class="mt-4" style="margin-left: 3rem">
            <h1 style="font-size: 4rem;" class="dark:text-white">Bienvenue aux Jeux de l'URCA 2024</h1>
            <p style="font-size: 1rem; width:50rem;" class="dark:text-gray-400">Participez aux Jeux de l'URCA 2024, une fusion de passion, performance et camaraderie, pour vivre des compétitions vibrantes et des célébrations mémorables.</p>
        </section>
    </div>
    <div class="sm:fixed" style="bottom: 30px; right: 30px; left: 30px;">
        <footer class="hidden justify-between space-x-8 sm:items-center sm:flex">
            <img src="{{ asset('storage/images/urca-logo.png') }}" style="height: 5rem" alt="Urca Logo" />
            <img src="{{ asset('storage/images/urca-games-logo.png') }}" style="height: 5rem" alt="Jeux Urca Logo" />
            <img src="{{ asset('storage/images/french-republic-logo.png') }}" style="height: 5rem" alt="Logo de la République Française" />
        </footer>
    </div>
</x-guest-layout>
