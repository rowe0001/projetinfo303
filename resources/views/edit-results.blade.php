<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Modifier les Resultats') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900 dark:text-gray-100">
                        {{ __("Ajouter") }}
                    </div>
                    <form id="add-result-form" action="{{ route('results.add') }}" method="POST" class="max-w-sm mx-auto">
                        @csrf
                        <div class="mb-4">
                            <select id="team" name="team" style="margin-left: 3rem; width: 92%;" class="form-select bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                @foreach ($teams as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <select id="event" name="event" style="margin-left: 3rem; width: 92%;" class="form-select bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                @foreach ($events as $event)
                                    <option value="{{ $event->id }}">{{ $event->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-2">
                            <label style="margin-left: 3rem; font-size:0.75rem;" class="dark:text-gray-100" for="position">Position</label>
                            <input id="position" name="position" style="margin-left: 3rem; width: 92%;" class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Position" required="">
                        </div>

                        <label style="margin-left: 3rem; font-size:0.75rem;" class="dark:text-gray-100" for="points">Points</label>
                        <input id="points" name="points" style="margin-left: 3rem; width: 92%;" class="form-control bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Points" required="">
                        
                        <p id="message-container-s" style="font-size: 0.75rem; color:green; margin-left:3rem; margin-top: 1rem; display:none;"></p>
                        <p id="message-container-e" style="font-size: 0.75rem; color:crimson; margin-left:3rem; margin-top: 1rem; display:none;"></p>
                        
                        <div class="p-6 text-gray-900 dark:text-gray-100">
                            <button onclick="addResult()" type="button" style="padding: 10px;" class="dark:bg-gray-900 inline-flex items-center px-5 py-2.5 mt-4 sm:mt-6 text-sm font-medium text-center text-white rounded-lg">Ajouter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        $(document).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return addResult();
                }
            });
        });

        function addResult() {
            $.ajax({
                type: 'POST',
                url: $('#add-result-form').attr('action'),
                data: $('#add-result-form').serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        // Redirect or display success message
                        $('#message-container-s').css('display', 'inline');
                        $('#message-container-s').text(response.message);
                        $('#message-container-e').css('display', 'none');
                    } else {
                        // Display error message
                        $('#message-container-e').css('display', 'inline');
                        $('#message-container-e').text(response.message);
                        $('#message-container-s').css('display', 'none');
                    }
                },
                error: function (xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    </script>
</x-app-layout>
