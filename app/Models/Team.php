<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'member_count',
        'password',
        'description',
        'points',
        'medals',
        'constitutive_id',
    ];

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }
}
