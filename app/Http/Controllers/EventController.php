<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function ordered() {
        $eventList = Event::where('start_date', '>', Carbon::now())
            ->orderBy('start_date', 'asc')
            ->get();

        return view('news', ['newsList' => $eventList]);
    }
}
