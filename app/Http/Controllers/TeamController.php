<?php

namespace App\Http\Controllers;

use App\Models\Constitutive;
use App\Models\Result;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TeamController extends Controller
{
    public function index()
    {
        return view('dashboard', ['teams' => Team::all()]);
    }

    public function join_team(Request $request)
    {
        $user = Auth::user();
        $selectedTeamId = $request->input('team');
        $password = $request->input('password');

        $team = Team::find($selectedTeamId);

        if ($user->team_id) {
            $currentTeam = Team::find($user->team_id);
            if ($currentTeam) {
                $currentTeam->decrement('member_count');
                $user->team()->dissociate();
                $user->save();
            }
        }

        if ($team->members_count >= 6) {
            return redirect()->route('teams.show')->with('error', 'The team is full.');
        }

        if ($team->members_count < 1) {
            $team->leader_id = $user->id;
            $team->save();
        }

        if ($team && Hash::check($password, $team->password)) {
            $team->increment('member_count');

            $user->team()->associate($team);
            $user->save();

            return response()->json(['success' => true, 'message' => 'You have joined the team successfully']);
        } else {
            return response()->json(['success' => false, 'message' => 'Incorrect password or team selection.']);
        }
    }

    public function ordered()
    {
        $teams = Team::orderByDesc('medals')
            ->orderByDesc('points')
            ->get();

        $leaders = User::whereIn('id', $teams->pluck('leader_id')->filter())->get()->keyBy('id');

        return view('ranks', ['teams' => $teams, 'leaders' => $leaders]);
    }

    public function data()
    {
        return view('edit-teams', ['constitutives' => Constitutive::all()]);
    }

    public function add(Request $request)
    {
        $user = Auth::user();
        $team_name = $request->input('name');
        $team_pswd = $request->input('password');
        $team_description = $request->input('description');
        $team_constitutive_id = $request->input('constitutive');

        $request->validate([
            'name' => 'required|string',
            'password' => 'required|string',
            'description' => 'required|string',
            'constitutive' => 'required|numeric'
        ]);

        // Create a new Result instance
        Team::create([
            'name' => $team_name,
            'member_count' => 0,
            'password' => Hash::make($team_pswd),
            'description' => $team_description,
            'points' => 0,
            'medals' => 0,
            'constitutive_id' => $team_constitutive_id,
        ],);

        return response()->json(['success' => true, 'message' => 'Successfully created new Team '.$team_name]);
    }
}
