<?php

namespace App\Http\Controllers;

use App\Models\Constitutive;
use App\Models\Event;
use App\Models\Result;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResultController extends Controller
{
    public function ordered_for_event($eventId)
    {
        $constitutiveRanking = DB::table('results')
            ->join('teams', 'results.team_id', '=', 'teams.id')
            ->where('results.event_id', $eventId)
            ->groupBy('teams.constitutive_id')
            ->select(
                'teams.constitutive_id',
                DB::raw('SUM(CASE WHEN results.position = 1 THEN 1 ELSE 0 END) as first_positions'),
                DB::raw('SUM(CASE WHEN results.position = 2 THEN 1 ELSE 0 END) as second_positions'),
                DB::raw('SUM(CASE WHEN results.position = 3 THEN 1 ELSE 0 END) as third_positions')
            )
            ->orderByDesc('first_positions')
            ->orderByDesc('second_positions')
            ->orderByDesc('third_positions')
            ->get();

        return $constitutiveRanking;
    }

    public function ordered()
    {
        $events = Event::all();
        $eventNames = [];
        $constitutives = [];
        $rankings = [];

        foreach($events as $event) {
            $eventNames[$event->id] = $event->name;
            $rankings[$event->id] = $this->ordered_for_event($event->id);
        }

        foreach(Constitutive::all() as $constitutive) {
            $constitutives[$constitutive->id] = $constitutive->name;
        }

        return view('results', ['rankings' => $rankings, 'events' => $eventNames, 'constitutives' => $constitutives]);
    }

    public function data()
    {
        return view('edit-results', ['teams' => Team::all(), 'events' => Event::all()]);
    }

    public function add(Request $request)
    {
        $team_id = $request->input('team');
        $points = $request->input('points');

        $request->validate([
            'team' => 'required|numeric',
            'event' => 'required|numeric',
            'position' => 'required|numeric',
            'points' => 'required|numeric'
        ]);

        // Create a new Result instance
        Result::create([
            'team_id' => $request->team,
            'event_id' => $request->event,
            'position' => $request->position
        ]);

        $team = Team::find($team_id);
        $team->increment('points', $points);

        return response()->json(['success' => true, 'message' => 'Successfully added new Result for '.$team->name]);
    }
}