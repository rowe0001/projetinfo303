<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\TeamController;
use App\Models\Team;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use LDAP\Result;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::get('/dashboard', [TeamController::class, 'index'])->name('dashboard');
    
    Route::get('/edit-results', [ResultController::class, 'data'])->middleware(['auth', 'verified'])->name('edit-results');
    Route::get('/edit-teams', [TeamController::class, 'data'])->middleware(['auth', 'verified'])->name('edit-teams');

    Route::get('/edit-users', function () {
        return view('edit-users');
    })->middleware(['auth', 'verified'])->name('edit-users');
});

Route::get('/news', [EventController::class, 'ordered'])->name('news.ordered');
Route::get('/ranks', [TeamController::class, 'ordered'])->name('ranks.ordered');
Route::get('/results', [ResultController::class, 'ordered'])->name('results.ordered');

Route::get('/dashboard/teams', function () {
    return view('dashboard-teams');
})->middleware(['auth', 'verified'])->name('users');

Route::post('/join', [TeamController::class, 'join_team'])->middleware(['auth', 'verified'])->name('join.team');
Route::post('/edit-results/add', [ResultController::class, 'add'])->middleware(['auth', 'verified'])->name('results.add');
Route::post('/edit-teams/add', [TeamController::class, 'add'])->middleware(['auth', 'verified'])->name('teams.add');

require __DIR__.'/auth.php';
