<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        DB::table('results')->insert([
            'team_id' => 1,
            'event_id' => 1,
            'position' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('results')->insert([
            'team_id' => 2,
            'event_id' => 1,
            'position' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('results')->insert([
            'team_id' => 1,
            'event_id' => 2,
            'position' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('results')->insert([
            'team_id' => 2,
            'event_id' => 2,
            'position' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
