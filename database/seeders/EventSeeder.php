<?php

namespace Database\Seeders;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $events = [
            [
                'name' => "HandBall 6/6",
                'description' => "Deux équipes de six joueurs s'affrontent pour marquer des buts en lançant un ballon dans le but adverse. Les joueurs utilisent leurs mains pour manier le ballon, et le rythme rapide du jeu en fait un sport intense et captivant. Il n'y aura pas de tribunes.",
                'location' => "GU (plateau C)",
                'start_date' => "2024-02-12 18:00:00",
                'end_date' => "2024-02-12 22:00:00",
                'max_attendees' => 8,
            ],
            [
                'name' => "Sumo",
                'description' => "Deux athlètes s'affrontent dans un cercle délimité. L'objectif est de forcer son adversaire à sortir du cercle ou à toucher le sol avec une autre partie du corps que les pieds. Les lutteurs, appelés sumotoris, sont souvent de grande taille et de poids imposant. Les équipes doivent être composées de 6 joueurs.",
                'location' =>"Salle de Crossfit ou salle principale",
                'start_date' =>"2024-02-12 18:00:00",
                'end_date' => "2024-02-12 22:00:00",
                'max_attendees' => 6
            ],
            [
                'name' => "Touch Rugby 4/4",
                'description' => "une version sans contact du rugby traditionnel. Les joueurs se passent le ballon en le touchant plutôt qu'en effectuant des plaquages. L'accent est mis sur la vitesse, la dextérité et la stratégie collective pour marquer des essais.",
                'location' => "",
                'start_date' => "2024-02-13 18:00:00",
                'end_date' => "2024-02-13 22:00:00",
                'max_attendees' => 8
            ],
            [
                'name' => "Laser Run",
                'description' => "Le Laser Run est une discipline du pentathlon moderne qui combine la course à pied et le tir au laser. Les participants alternent entre la course et le tir au laser, ajoutant une dimension tactique et physique au sport.",
                'location' => "En extérieur ou salle principale",
                'start_date' => "2024-02-13 18:00:00",
                'end_date' => "2024-02-13 22:00:00",
                'max_attendees' => 6
            ],
            [
                'name' => "Futsal",
                'description' => "Le futsal est une variante du football jouée en salle sur un terrain plus petit. Les équipes de cinq joueurs s'affrontent, mettant l'accent sur la rapidité, la technique et la créativité. Les surfaces de jeu réduites favorisent un jeu rapide et technique.",
                'location' => "",
                'start_date' => "2024-02-14 18:00:00",
                'end_date' => "2024-02-14 22:00:00",
                'max_attendees' => 8
            ],
            [
                'name' => "Palets bretons",
                'description' => "Les palets bretons sont un sport traditionnel de la Bretagne, en France. Il implique de lancer des palets en métal vers une cible, marquant des points en fonction de la précision et de la proximité des palets par rapport à la cible. les équipes doivent être composées de 3 à 6 joueurs.",
                'location' => "",
                'start_date' => "2024-02-14 18:00:00",
                'end_date' => "2024-02-14 22:00:00",
                'max_attendees' => 6
            ],
            [
                'name' => "Finales",
                'description' => "",
                'location' => "GU et salle de Crossfit",
                'start_date' => "2024-02-15 13:00:00",
                'end_date' => "2024-02-15 18:00:00",
                'max_attendees' => 0
            ],
            [
                'name' => "Badminton par équipes de 6",
                'description' => "Le badminton est un sport de raquette où les joueurs s'affrontent en simple (un contre un) ou en double (deux contre deux). Le but est de marquer des points en faisant passer le volant au-dessus du filet et en le faisant atterrir dans le camp adverse.",
                'location' => "",
                'start_date' => "2024-02-19 18:00:00",
                'end_date' => "2024-02-19 22:00:00",
                'max_attendees' => 36
            ],
            [
                'name' => "Relais Crossfit",
                'description' => "Le relais Crossfit est une compétition d'équipe où les membres alternent entre différentes épreuves de fitness, combinant des exercices de force, d'endurance et de gymnastique. Les équipes tentent de compléter le parcours dans le temps le plus rapide. Les équipes doivent être composées de 6 joueurs.",
                'location' => "",
                'start_date' => "2024-02-19 18:00:00",
                'end_date' => "2024-02-19 22:00:00",
                'max_attendees' => 6
            ],
            [
                'name' => "Volley 6/6",
                'description' => "Le volley 6/6 est un sport d'équipe où deux équipes de six joueurs s'affrontent en essayant de faire passer un ballon au-dessus du filet dans le camp adverse. La coordination et la communication sont essentielles pour réussir à marquer des points.",
                'location' => "",
                'start_date' => "2024-02-20 18:00:00",
                'end_date' => "2024-02-20 22:00:00",
                'max_attendees' => 18
            ],
            [
                'name' => "Relais Marathon par équipe",
                'description' => "Le relais marathon par équipe consiste à diviser la distance d'un marathon entre plusieurs coureurs au sein d'une équipe. Chaque coureur court une partie de la distance, et l'équipe travaille ensemble pour compléter le marathon dans le temps le plus rapide possible.",
                'location' => "",
                'start_date' => "2024-02-20 18:00:00",
                'end_date' => "2024-02-20 22:00:00",
                 'max_attendees' => 6
            ],
            [
                'name' => "Tournois e-sport",
                'description' => "Les tournois e-sport sont des compétitions de jeux vidéo organisées à un niveau professionnel. Les joueurs s'affrontent dans des jeux populaires, souvent en équipes, mettant en avant leurs compétences stratégiques et leur maîtrise du jeu. Les équipes doivent être composées de 6 joueurs",
                'location' => "CREPS",
                'start_date' => "2024-02-20 18:00:00",
                'end_date' => "2024-02-20 22:00:00",
                'max_attendees' => 10
            ],
            [
                'name' => "Basket 3/3",
                'description' => "Le basket 3/3 est une version du basketball traditionnel jouée avec trois joueurs de chaque côté. Les équipes s'affrontent sur un demi-terrain, mettant en avant la rapidité, la précision et le jeu en équipe. Les équipes doivent être constituées de 6 joueurs",
                'location' => "plateau HCR 1/2 salle",
                'start_date' => "2024-02-21 18:00:00",
                'end_date' => "2024-02-21 22:00:00",
                'max_attendees' => 12
            ],
            [
                'name' => "Escalade de Vitesse",
                'description' => "Les grimpeurs compétitionnent pour atteindre le sommet d'une paroi le plus rapidement possible. La rapidité et la maîtrise des mouvements sont cruciales dans cette forme intense de l'escalade. La traversée est compléter en relais dans des équipes de 3 à 6 joueurs",
                'location' => "",
                'start_date' => "2024-02-21 18:00:00",
                'end_date' =>"2024-02-21 22:00:00",
                'max_attendees' => 6
            ],
            [
                'name' => "Finales",
                'description' => "",
                'location' => "",
                'start_date' => "2024-02-22 13:00:00",
                'end_date' => "2024-02-22 18:00:00",
                'max_attendees' => 0
            ],
            [
                'name' => "Cérémonie de cloture",
                'description' => "",
                'location' => "",
                'start_date' => "2024-02-22 18:00:00",
                'end_date' => "2024-02-22 21:00:00",
                'max_attendees' => 0
            ]
        ];

        foreach($events as $event) {
            Event::create($event);
        }
    }
}
