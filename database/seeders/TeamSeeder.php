<?php

namespace Database\Seeders;

use App\Models\Constitutive;
use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $constitutiveIds = Constitutive::pluck('id');

        $teams = [
            [
                'name' => 'Team A',
                'member_count' => 0,
                'password' => Hash::make('password'),
                'description' => 'Description for Team A.',
                'points' => 0,
                'medals' => 0,
                'constitutive_id' => $constitutiveIds->random(),
            ],
            [
                'name' => 'Team B',
                'member_count' => 0,
                'password' => Hash::make('password'),
                'description' => 'Description for Team B.',
                'points' => 0,
                'medals' => 0,
                'constitutive_id' => $constitutiveIds->random(),
            ],
        ];

        // Populate the teams table
        foreach ($teams as $team) {
            Team::create($team);
        }
    }
}
