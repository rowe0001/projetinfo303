<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::create([
            'name' => 'admin', 
            'email' => 'admin@test.com',
            'password' => bcrypt('password')
        ]);

        $admin->assignRole('administrator');

        $organizer = User::create([
            'name' => 'organizer', 
            'email' => 'organizer@test.com',
            'password' => bcrypt('password')
        ]);

        $organizer->assignRole('organizer');

        $user = User::create([
            'name' => 'user', 
            'email' => 'user@test.com',
            'password' => bcrypt('password')
        ]);
    }
}
