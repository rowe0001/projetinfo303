<?php

namespace Database\Seeders;

use App\Models\Constitutive;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ConstitutiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $constitutives = [
            'SEN',
            'LSH',
            'STAPS',
            'DSP',
            'CDC',
            'ESIReims',
            'Institut G. Chappaz',
            'EiSINe',
            'SESG',
            'Médecine',
            'Pharma',
            'Odonto',
            'IUT RCC',
            'Inspé',
            'Siège',
            'IUT Troyes',
        ];

        foreach ($constitutives as $constitutive) {
            Constitutive::create(['name' => $constitutive]);
        }
    }
}
