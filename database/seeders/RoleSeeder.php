<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role = Role::create(['name' => 'administrator']);
        $role->syncPermissions(Permission::create(['name' => 'admin']));

        $role = Role::create(['name' => 'organizer']);
        $role->syncPermissions(Permission::create(['name' => 'organize']));
    }
}
