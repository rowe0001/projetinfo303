<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('member_count');
            $table->string('password');
            $table->text('description');
            $table->integer('points');
            $table->integer('medals');
            $table->unsignedBigInteger('constitutive_id');
            $table->timestamps();

            $table->foreign('constitutive_id')->references('id')->on('constitutives');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('teams');
    }
};
